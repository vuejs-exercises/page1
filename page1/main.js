// Constante creada para llamar la nueva clase Vue
const app = new Vue({
    // ID del elemento dónde se trabajarán los datos y métodos actuales
    el: '#app',
    // Variables, Objetos y Arrays que se utilizarán en la vista
    data: {
        titulo: 'Hola mundo con Vue',
        parrafo: '',
        frutas: ['Banano', 'Manzana', 'Pera', 'Mandarina'],
        Objetos: [
            {nombre: 'Samuel', Apellido: 'Toledo', Edad: 23},
            {nombre: 'Khirbet', Apellido: 'Bautista', Edad: 17}
        ],        
        Opciones: [],
        nuevaFruta: ''
    },
    // Métodos que se utilizarán en la vista
    methods: {
        /* se puede declarar un nuevo método y su función:
        nombreMetodo: function () {}
        o hacerlo de forma abreviada: 
        nombreMetodo () {} */
        agregarFruta () {
            // Al utilizar una variable creada en Data es necesario colocar This
            this.frutas.push(this.nuevaFruta);
            this.Objetos.push({
                nombre: this.nuevaFruta, Apellido: 'Fruta', Edad: 10
            })
        },
        eliminarFruta () {

        }
    }
})